<?php

class Members_Model extends CI_Model {

	function get_all_members() {
		$this->db->select(" a.*, concat('VITR-', LPAD(a.member_id, 4, 0)) as vit_id");
		$this->db->from("va_members as a");
		$result = $this->db->get()->result();
		return $result;
	}

	function check_member_id($id) {
		$this->db->select(" a.*, concat('VITR-', LPAD(a.member_id, 4, 0)) as vit_id");
		$this->db->from("va_members as a");
		$this->db->where("(SELECT concat('VITR-', LPAD(b.member_id, 4, 0)) FROM va_members as b WHERE a.member_id = b.member_id) =", $id);
		$result = $this->db->get()->row();
		return $result;
	}

	function update_member($id, $data) {
		$this->db->where('member_id', $id);
		$this->db->update('va_members', $data);
		return $this->db->affected_rows();
	}

	function get_member_activation($activation_code) {
		$this->db->select(" a.*, concat('VITR-', LPAD(a.member_id, 4, 0)) as vit_id");
		$this->db->from("va_members as a");
		$this->db->where("a.member_activation_code", $activation_code);
		$result = $this->db->get()->row();
		return $result;
	}

	function get_child_info($id) {
		$this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_child', $id);
		return $this->db->get()->row();
	}

	function get_member_info_id($id) {
		$this->db->select(" a.*, concat('VITR-', LPAD(a.member_id, 4, 0)) as vit_id");
		$this->db->from("va_members as a");
		$this->db->where("a.member_id", $id);
		$result = $this->db->get()->row();
		return $result;
	}

	function get_member_downline($id) {
		$this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_parent', $id);
		$this->db->order_by('relation_position', 'asc');
		$this->db->limit('2');
		return $this->db->get()->result();
	}

	function get_member_level($id) {
		$this->db->select('*');
		$this->db->from('va_member_relation');
		$this->db->where('relation_child', $id);
		$this->db->order_by('relation_position', 'asc');
		$this->db->limit('2');
		return $this->db->get()->row();
	}

	function insert_member($data) {
		$this->db->insert('va_members', $data);
		return $this->db->insert_id();
	}

	function insert_relation($data) {
		$this->db->insert('va_member_relation', $data);
		return $this->db->insert_id();
	}

	function get_points($member_id, $month) {
		$date123 = "%Y";
    	$year = mdate($date123, time());
		$this->db->select("(SELECT SUM(point_value) FROM va_member_points WHERE MONTH(va_member_points.date_created) = '$month' AND member_id = '$member_id' AND YEAR(va_member_points.date_created) = '$year'  GROUP BY member_id) as point_value", false);
		return $this->db->get('va_member_points')->row();
	}

	function insert_points($data) {
		$this->db->insert('va_member_points', $data);
		return $this->db->insert_id();
	}

	function get_member_franchise($member_franchise_code) {
		$this->db->select(" a.*, concat('VITR-', LPAD(a.member_id, 4, 0)) as vit_id");
		$this->db->from("va_members as a");
		$this->db->where('member_franchise_code', $member_franchise_code);
		$result = $this->db->get()->result();
		return $result;
	}

	function get_all_payouts_franchise($member_franchise_code) {
		$this->db->select(" a.*, b.*, concat('VITR-', LPAD(a.member_id, 4, 0)) as vit_id");
		$this->db->from("va_members as a");
		$this->db->join("va_payouts as b", "b.member_id = a.member_id");
		$this->db->where('member_franchise_code', $member_franchise_code);
		#this->db->where('b.payout_admin_status', '0');
		$this->db->where("b.payout_status", "0");
		$result = $this->db->get()->result();
		return $result;
	}

	function get_all_payouts_franchise_all() {
		$this->db->select("@n := @n + 1 n, SUM(a.payout_amount) as total_amount, a.date_created as date_created, c.franchiser_code as franchiser_code, c.franchiser_location as franchiser_location,
c.franchiser_fname as franchiser_fname, c.franchiser_lname as franchiser_lname, c.franchiser_id as franchiser_id, group_concat(a.payout_id) as payout_id", false);
		$this->db->from("(SELECT @n := 0) m, va_payouts as a", false);
		$this->db->join("va_members as b", "a.member_id = b.member_id");
		$this->db->join("va_franchisers as c", "b.member_franchise_code = c.franchiser_id");
		$this->db->where("a.payout_admin_status" , '1');
		$this->db->group_by("c.franchiser_id, a.date_created");
		$result = $this->db->get()->result();
		return $result;
	}

	function get_all_payouts_franchise_receive($fracnhise_id) {
		$this->db->select("@n := @n + 1 n, SUM(a.payout_amount) as total_amount, a.date_created as date_created, c.franchiser_code as franchiser_code, c.franchiser_location as franchiser_location,
c.franchiser_fname as franchiser_fname, c.franchiser_lname as franchiser_lname, c.franchiser_id as franchiser_id, group_concat(a.payout_id) as payout_id", false);
		$this->db->from("(SELECT @n := 0) m, va_payouts as a", false);
		$this->db->join("va_members as b", "a.member_id = b.member_id");
		$this->db->join("va_franchisers as c", "b.member_franchise_code = c.franchiser_id");
		$this->db->where('c.franchiser_id', $fracnhise_id);
		$this->db->where("a.payout_admin_status" , '0');
		$this->db->group_by("c.franchiser_id, a.date_created");
		$result = $this->db->get()->result();
		return $result;
	}

	function get_all_payouts_franchise_spe($id) {
		$this->db->select("@n := @n + 1 n, SUM(a.payout_amount) as total_amount, a.date_created as date_created, c.franchiser_code as franchiser_code, c.franchiser_location as franchiser_location,
c.franchiser_fname as franchiser_fname, c.franchiser_lname as franchiser_lname, a.payout_id as payout_id, c.franchiser_id as franchiser_id, group_concat(a.payout_id) as payout", false);
		$this->db->from("(SELECT @n := 0) m, va_payouts as a", false);
		$this->db->join("va_members as b", "a.member_id = b.member_id");
		$this->db->join("va_franchisers as c", "b.member_franchise_code = c.franchiser_id");
		$this->db->group_by("c.franchiser_id, a.date_created");
		$this->db->having('n =' , $id, false);
		$result = $this->db->get()->row();
		return $result;
	}

	function insert_pdc_payout($data) {
		$this->db->insert('va_pdc_payout', $data);
		return $this->db->insert_id();
	}

	// Payout

		function get_all_payouts($member_id) {
			$this->db->where('member_id', $member_id);
			return $this->db->get('va_payouts')->result();
		}

		function update_payout($member_id, $payout_id, $data) {
			$this->db->where('member_id', $member_id);
			$this->db->where('payout_id', $payout_id);
			$this->db->update('va_payouts', $data);
			return $this->db->affected_rows();
		}

		function update_payout_force($payout_id, $data) {
			$this->db->where('payout_id', $payout_id);
			$this->db->update('va_payouts', $data);
			return $this->db->affected_rows();
		}

		function get_pairing($member_id, $date) {
			$this->db->where('member_id', $member_id);
			$this->db->where("DATE_FORMAT(date_created,'%m-%d-%Y') =", $date);
			return $this->db->get('va_pairings')->row();
		}

		function get_mlm($member_id, $date) {
			$this->db->where('member_id', $member_id);
			$this->db->where("DATE_FORMAT(date_created,'%m-%d-%Y') =", $date);
			return $this->db->get('va_mlms')->row();
		}

		function get_pairing_cha() {
			return $this->db->get('va_pairings')->result();
		}

		function get_mlm_cha() {
			return $this->db->get('va_mlms')->result();
		}

		function chart_pairing() {
			$this->db->select('*, SUM(pairing_amount) as pairing_amounts');
			$this->db->from('va_pairings as a');
			$this->db->group_by("month(a.date_created)");
			return $this->db->get()->result();
		}

		function chart_mlm() {
			$this->db->select('*, SUM(mlm_amount) as mlm_amounts');
			$this->db->from('va_mlms as a');
			$this->db->group_by("month(a.date_created)");
			return $this->db->get()->result();
		}

		function get_chart_data() {
			$this->db->select('*, SUM(pairing_amount) as pairing_amount, SUM(mlm_amount) as mlm_amount');
			$this->db->from('va_pairings as a');
			$this->db->join('va_mlms as b', 'b.date_created = a.date_created');
			$this->db->group_by("month(b.date_created)");
			return $this->db->get()->result();
		}

		function get_member_by_pdc($franchiser_id) {
			$this->db->select('SUM(a.payout_amount) as total_amount', false);
			$this->db->from('va_payouts as a');
			$this->db->join('va_members as b', 'b.member_id = a.member_id');
			$this->db->where('b.member_franchise_code', $franchiser_id);
			return $this->db->get()->row();
		}


		///////////////////////

		function testing($member_id) {
			echo $this->Computation_Model->get_mlm_indi($member_id);
		}

}
