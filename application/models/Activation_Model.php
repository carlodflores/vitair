<?php

class Activation_Model extends CI_Model {

    function get_activation_by_franchise($franchise_id) {
        $this->db->where('franchiser_id', $franchise_id);
        return $this->db->get('va_activations')->result();
    }

    function get_activation_all() {
        return $this->db->get('va_activations')->result();
    }

    function check_valid_code($activation_code) {
        $this->db->where('activation_status', '1');
        $this->db->where('activation_code', $activation_code);
        return $this->db->get('va_activations')->row();
    }

    function get_activation_by_status($status,$franchise_id) {
        $this->db->where('activation_status', $status);
        $this->db->where('franchiser_id', $franchise_id);
        return $this->db->get('va_activations')->result();
    }

    function update_code($activation_code, $data) {
        $this->db->where('activation_code', $activation_code);
        $this->db->update('va_activations', $data);
        return $this->db->affected_rows();
    }

    function insert_activation($data) {
        $this->db->insert('va_activations', $data);
        return $this->db->insert_id();
    }

}
