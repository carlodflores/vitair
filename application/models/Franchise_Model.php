<?php

class Franchise_Model extends CI_Model {

    function get_all_franchisers() {
        return $this->db->get("va_franchisers")->result();
    }

    function get_franchise_by_id($franchise_id) {
        $this->db->where("franchiser_code", $franchise_id);
        return $this->db->get("va_franchisers")->row();
    }

    function get_franchise_id($franchise_id) {
        $this->db->where("franchiser_id", $franchise_id);
        return $this->db->get("va_franchisers")->row();
    }

    function insert_franchiser($data) {
        $this->db->insert('va_franchisers', $data);
        return $this->db->insert_id();
    }

    function update_franchiser($data, $franchise_id) {
        $this->db->where("franchiser_id", $franchise_id);
        $this->db->update('va_franchisers', $data);
        return $this->db->insert_id();
    }

    // Stocks

        function check_stock($product_id, $franchiser_id) {
            $this->db->where('franchiser_id', $franchiser_id);
            $this->db->where('product_id', $product_id);
            return $this->db->get('va_franchiser_stocks')->row();
        }

        function insert_stock($data) {
            $this->db->insert('va_franchiser_stocks', $data);
            return $this->db->insert_id();
        }

        function update_stock($data, $franchiser_id, $product_id) {
            $this->db->where('franchiser_id', $franchiser_id);
            $this->db->where('product_id', $product_id);
            $this->db->update('va_franchiser_stocks', $data);
            return $this->db->insert_id();
        }

    // locations
        function get_location_by_code($code) {
            $this->db->where('location_short_code', $code);
            return $this->db->get("va_franchiser_locations")->result();
        }

        function get_all_locations() {
            return $this->db->get("va_franchiser_locations")->result();
        }

        function insert_location($data) {
            $this->db->insert('va_franchiser_locations', $data);
            return $this->db->insert_id();
        }

}
