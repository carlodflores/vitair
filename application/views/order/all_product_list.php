<?php
	$this->load->view('header_view');
	$this->load->view('left_panel_view');

	$id = $this->session->userdata('user')['member_id'];

	$mi = $this->Members_Model->get_member_info_id($id);

	$this->Computation_Model->get_pairing($id);
	$this->Computation_Model->get_pairing_count($id);
	$this->Computation_Model->compute_pairing_bonus($id);
	$pdc_id_curr = isset($pdc_id) ? $this->Franchise_Model->get_franchise_by_id($pdc_id)->franchiser_id : $mi->member_franchise_code;
 ?>


    <div class="main-panel">
		<?php $this->load->view('top_nav_view'); ?>
        <div class="content">
			<br/><br/><br/>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="exampleInputEmail1">Product Distribution Center</label>
							<select class="form-control" name="txt_pdc">
								<?php foreach ($this->Franchise_Model->get_all_franchisers() as $key => $value): ?>
									<option value="<?=base_url() . 'order/out/' .$value->franchiser_code?>"><?=$value->franchiser_code?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
			</div>
            <div class="container-fluid">
                <div class="row">
                    <?php foreach ($this->Order_Model->get_franchise_stocks($pdc_id_curr) as $key => $row): ?>
						<?php
							$value = $this->Product_Model->get_product_details($row->product_id);
						 ?>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <center>
                                                <div class="img-holder" style="height: 300px; overflow: hidden;">
                                                    <img src="<?=base_url()?>assets/<?=$value->product_file?>" width="200" alt="">
                                                </div>
                                                <div style="height: 50px;"><h6><?=$value->product_name?></h6><Br/></div>
                                                <a href="<?=base_url()?>order/add/<?=$value->product_id?>/">
													<button type="submit" class="btn btn-info btn-fill">Order</button>
												</a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
<script>
	$(function(){
	  // bind change event to select
	  $('select[name="txt_pdc"]').on('change', function () {
		  var url = $(this).val(); // get selected value
		  if (url) { // require a URL
			  window.location = url; // redirect
		  }
		  return false;
	  });
	});
</script>
