<?php
	$this->load->view('header_view');
	$this->load->view('left_panel_view');

	$id = $this->session->userdata('user')['member_id'];
	$this->Computation_Model->get_pairing($id);
	$this->Computation_Model->get_pairing_count($id);
	$this->Computation_Model->compute_pairing_bonus($id);
 ?>


    <div class="main-panel">
		<?php $this->load->view('top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <div style="float :left;">
                                    <h4 class="title">Cart</h4>
                                    <p class="category">Your cart content.</p>
                                </div>
                                <?php if (count($carts) != 0): ?>
									<a href="<?=base_url()?>order/place/<?=$group?>/"><button class="btn btn-info btn-fill pull-right">Place Order</button></a>
                                <?php endif; ?>
                            </div>
							<form action="<?=base_url()?>order/updatecart/" method="post">
								<input class="btn btn-info btn-fill pull-right" style="margin-right: 20px;" type="submit" value="Update Cart">
								<div class="clearfix"></div>
                            <div class="content table-responsive table-full-width">
								<table class="table table-hover table-striped">
									<thead>
										<th></th>
										<th>Product</th>
										<th>Quantity</th>
										<th>Price</th>
										<th>Total</th>
									</thead>
									<tbody>
										<?php foreach ($carts as $key => $cart): ?>
											<?php
												$pd = $this->Product_Model->get_product_details($cart->product_id);
											 ?>
											<tr>
												<td></td>
												<td><?=$pd->product_name?></td>
												<td>
													<input type="hidden" name="cart_id[]" value="<?=$cart->cart_id?>">
													<input class="form-control" min="1" name="cart_qty[]" style="width: 20%;" type="number" value="<?=$cart->cart_quantity?>" />
												</td>
												<td><?=$pd->product_price?></td>
												<td>P <?=number_format(intval($cart->cart_quantity)*intval($pd->product_price), 2)?></td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
                            </div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
