<?php
	$this->load->view('header_view');
	$this->load->view('left_panel_view');

	$id = $this->session->userdata('user')['member_id'];

	$date = "%m";
	$month = mdate($date, time());
	$point_value = isset($this->Members_Model->get_points($id, $month)->point_value) ? $this->Members_Model->get_points($id, $month)->point_value : 0;
	$level_points = $this->Computation_Model->get_level_points();
	$mlm_bonus = isset($mlm_bonus)?	$mlm_bonus : 0;

	$first_day_this_month = date('m-01-Y'); // hard-coded '01' for first day
	$last_day_this_month  = date('m-t-Y');
 ?>


    <div class="main-panel">
		<?php $this->load->view('top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Total MLM Income</h4>
                                <p class="category">from <?=$first_day_this_month?> to <?=$last_day_this_month?></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <tbody>

										<tr>
                                        	<td>Direct Referral Bonus</td>
                                        	<td></td>
                                        	<td><?=/*number_format($this->Computation_Model->get_direct_referral($id), 2)*/ '- - - - - '?></td>
                                        </tr>
										<tr style="background: #f9f9f9;">
                                        	<td>MLM - Personal</td>
                                        	<td></td>
                                        	<td>₱  <?=number_format((1*0.05*$point_value), 2)?></td>
                                        </tr>
										<tr>
                                        	<td>MLM - Group</td>
                                        	<td></td>
                                        	<td>₱ <?=number_format($this->Computation_Model->get_mlm_bonus()-(1*0.05*$point_value), 2)?></td>
                                        </tr>
										<tr style="background: #fff;">
                                        	<td style="padding-top: 10px;">Current MLM Bonus Income</td>
                                        	<td style="padding-top: 10px;"></td>
											<?php
											#/*$this->Computation_Model->get_direct_referral($this->session->userdata('user')['member_id'])+*/
											 ?>
                                        	<td style="padding-top: 10px;">₱ <?=number_format($this->Computation_Model->get_mlm_bonus(), 2)?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

					<div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Total Pairing Income</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <tbody>
										<tr style="background: #fff;">
                                        	<td>Current Pairing Bonus</td>
                                        	<td></td>
                                        	<td>₱ <?=number_format($this->Computation_Model->get_left_pairing_bonus(), 2)?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

					<div class="col-md-12">
						<div class="card">
							<div class="header">
                                <h4 class="title">Pairing & MLM Bonus</h4>
                                <p class="category">Your Income details and Status</p>
                            </div>
                            <div class="content table-responsive table-full-width">
							    <table id="pmlmbonus" class="table table-hover table-striped">
									<thead>
										<th>Payout Date</th>
										<th>Pairing Bonus</th>
										<th>MLM</th>
										<th>Total Payout</th>
										<th>Status</th>
									</thead>
									<tbody>
										<?php foreach ($this->Members_Model->get_all_payouts($id) as $key => $value): ?>
											<?php
												$date_create = date('m-d-Y', strtotime($value->date_created));
											 ?>
											<tr>
												<td><?=$value->date_created?></td>
												<td><?=isset($this->Members_Model->get_pairing($id, $date_create)->pairing_amount)? $this->Members_Model->get_pairing($id, $date_create)->pairing_amount : "0.00"?></td>
												<td><?=isset($this->Members_Model->get_mlm($id, $date_create)->mlm_amount)? $this->Members_Model->get_mlm($id, $date_create)->mlm_amount : "0.00"?></td>
												<td><?=$value->payout_amount?></td>
												<td>
													<?php if ($value->payout_status == 1): ?>
														Unclaimed
													<?php else: ?>
														Claimed
													<?php endif; ?>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#pmlmbonus').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>


<?php if (isset($_GET['success']) && $_GET['success']  == md5('order')): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully placed your order."

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
