<footer class="footer" style="background: url('<?=base_url()?>assets/img/Footer-2.png'); " >
    <div class="container-fluid">
        <center>
            <p class="copyright" style="color: #fff;">
                2016 <a href="#">Airoshere Oils Corporation</a>
            </p>
        </center>
    </div>
</footer>

</div>
</div>


</body>
<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.qtip.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
<!--   Core JS Files   -->
<script src="<?=base_url()?>assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/jquery.qtip.js"></script>


<!--  Checkbox, Radio & Switch Plugins -->
<script src="<?=base_url()?>assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="<?=base_url()?>assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="<?=base_url()?>assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="<?=base_url()?>assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="<?=base_url()?>assets/js/demo.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    demo.initChartist();
});
</script>

</html>
