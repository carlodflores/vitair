<?php
	$this->load->view('franchise/header_view');
	$this->load->view('franchise/left_panel_view');
 ?>


    <div class="main-panel">
		<?php $this->load->view('franchise/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Register Account</h4>
                            </div>
                            <div class="content">
								<form class="" action="<?=base_url()?>franchise/account/" method="post">
								<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">First Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=set_value('txt_fname')?>" /><br/>
										<?=form_error('txt_fname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Last Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_lname" placeholder="Last Name" value="<?=set_value('txt_lname')?>" /><br/>
										<?=form_error('txt_lname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Direct Sponsor ID <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_referral_id" placeholder="Direct Referral ID" value="<?=set_value('txt_referral_id')?>" /><br/>
										<?=form_error('txt_referral_id', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">PDC ID<span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_pcd_id" placeholder="Product Distribution Center ID" value="<?=$this->session->userdata('franchise')['franchise_code']?>" readonly /><br/>
										<?=form_error('txt_pcd_id', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Activation code <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_activation_code" placeholder="Activation code" value="<?=set_value('txt_activation_code')?>"/><br/>
										<?=form_error('txt_activation_code', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Account Position <span class="text-danger">*</span></label>
        								<select class="form-control" name="txt_position">
											<option value="">Select Account Position</option>
											<option value="L" <?= set_select('txt_position', 'L', TRUE)?>>Left</option>
											<option value="R" <?= set_select('txt_position', 'R', TRUE)?>>Right</option>
										</select><br/>
										<?=form_error('txt_position', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Password <span class="text-danger">*</span></label>
        								<input type="password" class="form-control" name="txt_password" placeholder="Relation to the Beneficiary" value="<?=set_value('txt_password')?>" /><br/>
										<?=form_error('txt_password', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Confirm Password <span class="text-danger">*</span></label>
        								<input type="password" class="form-control" name="txt_re_password" placeholder="Relation to the Beneficiary" value="<?=set_value('txt_re_password')?>" /><br/>
										<?=form_error('txt_re_password', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

								<hr>

                                <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Contact #</label>
                								<input type="text" class="form-control" name="txt_contact" placeholder="Contact #" value="<?=set_value('txt_contact')?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                								<input type="text" class="form-control" name="txt_email" placeholder="Email" value="<?=set_value('txt_email')?>" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Date of Birth</label>
                								<input type="date" class="form-control" name="txt_birth" placeholder="Date of Birth" value="<?=set_value('txt_birth')?>" />
                                            </div>
                                        </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Address</label>
            								<input type="text" class="form-control" name="txt_address" placeholder="Full Name" value="<?=set_value('txt_address')?>"/>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Name of Beneficiary</label>
        								<input type="text" class="form-control" name="txt_beneficiary" placeholder="Name of Beneficiary (Full Name)" value="" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Relation</label>
        								<input type="text" class="form-control" name="txt_relation" placeholder="Relation to the Beneficiary" value="" />
                                        </div>
                                    </div>
                                </div>
								<hr>
                                <Br/>
								<button type="submit" class="btn btn-info btn-fill pull-right">Save</button>
								<div class="clearfix"></div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
<?php if (isset($_GET['success'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully added a member"

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
