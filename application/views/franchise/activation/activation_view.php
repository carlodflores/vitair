<?php
	$this->load->view('franchise/header_view');
	$this->load->view('franchise/left_panel_view');
 ?>


    <div class="main-panel">
		<?php $this->load->view('franchise/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Activation Codes</h4>
                                <p class="category">Manage activation codes for head office and franchise.</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>#</th>
                                        <th>Activation Code</th>
                                        <th>Date Realased</th>
                                        <th>Status</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($activation as $key => $value): ?>
											<tr>
	                                        	<td><?=$value->activation_id?></td>
	                                        	<td><?=$value->activation_code?></td>
	                                        	<td><?=$value->date_created?></td>
	                                            <td>
													<?php if ($value->activation_status == 0): ?>
														Unavailable
													<?php else: ?>
														Available
													<?php endif; ?>
												</td>
	                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
<script type="text/javascript">
	$(function() {
		$('table').bootstrapTable({
            toolbar: ".toolbar",
            clickToSelect: true,
            showRefresh: true,
            search: true,
            showToggle: true,
            showColumns: true,
            pagination: true,
            searchAlign: 'left',
            pageSize: 8,
            clickToSelect: false,
            pageList: [8,10,25,50,100],

            formatShowingRows: function(pageFrom, pageTo, totalRows){
                //do nothing here, we don't want to show the text "showing x of y from..."
            },
            formatRecordsPerPage: function(pageNumber){
                return pageNumber + " rows visible";
            },
            icons: {
                refresh: 'fa fa-refresh',
                toggle: 'fa fa-th-list',
                columns: 'fa fa-columns',
                detailOpen: 'fa fa-plus-circle',
                detailClose: 'fa fa-minus-circle'
            }
        });
	});
</script>
