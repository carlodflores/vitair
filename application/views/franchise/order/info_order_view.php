<?php
	$this->load->view('franchise/header_view');
	$this->load->view('franchise/left_panel_view');
 ?>


    <div class="main-panel">
		<?php $this->load->view('franchise/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-8">
						<div class="card">
							<div class="header">
                                <h4 class="title">Order Content</h4>
                                <p class="category">The Products included in Order.</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped sortid">
    								<thead>
    									<tr>
    										<th></th>
    										<th data-field="name" data-sortable="true">Product Name</th>
                                            <th data-field="price" data-sortable="true">Price</th>
                                            <th data-field="points" data-sortable="true">Quantity</th>
    										<th data-field="date" data-sortable="true">Total</th>
    									</tr>
    								</thead>
    								<tbody>
                                        <?php foreach ($content as $key => $value): ?>
                                            <tr>
                                                <?php
                                                    $pd = $this->Product_Model->get_product_details($value->product_id);
                                                 ?>
                                                <td style="text-align: center; ">
                                                    <center>
                                                        <img src="<?=base_url()?>assets/<?=$pd->product_file?>" width="60" alt="" />
                                                    </center>
                                                </td>
                                                <td><?=$pd->product_name?></td>
                                                <td><?=$pd->product_price?></td>
                                                <td><?=$value->cart_quantity?></td>
                                                <td>P <?=number_format(intval($pd->product_price)*intval($value->cart_quantity), 2)?></td>
                                            </tr>
                                        <?php endforeach; ?>
    								</tbody>
    							</table>
                            </div>
						</div>
					</div>

                    <div class="col-md-4">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Order Summary</h4>
                                <p class="category">The Order Summary</p>
                            </div>

                            <div class="content">
                                <div class="form-group">
                                    <?php
                                        $mi = $this->Members_Model->get_member_info_id($value->member_id);
                                     ?>

                                     <label for="exampleInputEmail1">Member ID</label>
                                     <input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=$mi->vit_id?>" readonly /><br/>
                                     <?=form_error('txt_fname', '<p class="text-danger">', '</p>')?>

                                    <label for="exampleInputEmail1">Member Full Name</label>
                                    <input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=$mi->member_fname .  ' ' . $mi->member_lname?>" readonly /><br/>
                                    <?=form_error('txt_fname', '<p class="text-danger">', '</p>')?>

                                    <label for="exampleInputEmail1">Order Total</label>
                                    <input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="P <?=number_format($order->order_total, 2)?>" readonly /><br/>
                                    <?=form_error('txt_fname', '<p class="text-danger">', '</p>')?>

                                    <label for="exampleInputEmail1">Order Total Points</label>
                                    <input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=$order->order_total_points?>" readonly /><br/>
                                    <?=form_error('txt_fname', '<p class="text-danger">', '</p>')?>

                                    <label for="exampleInputEmail1">Order Status</label>
                                    <input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=$order->order_status == 1 ? 'Pending' : 'Done'?>" readonly /><br/>
                                    <?=form_error('txt_fname', '<p class="text-danger">', '</p>')?>

                                    <?php if ($order->order_status != 2): ?>
										<Br/>
	    								<a href="<?=base_url()?>franchise/order/mark/done/<?=$order->order_id?>/">
	                                        <button type="button" class="btn btn-info btn-fill pull-right">Mark Order as Done</button>
	                                    </a>
                                    <?php endif; ?>
    								<div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],

                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>
<?php if (isset($_GET['success'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully served an Order."

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>

<?php if (isset($_GET['fail'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-close-circle',
				<?php if(md5('markdone') == $_GET['fail']): ?>
					message: "Some error occur while marking the order."
				<?php endif; ?>

				<?php if(md5('insustock') == $_GET['fail']): ?>
					message: "You have insufficient Stock to make this order."
				<?php endif; ?>

			},{
				type: 'danger',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
