<?php
    $group = "";
    $member_id = $this->session->userdata('user')['member_id'];
    if($this->session->userdata('cart') !== null) {
        $group = $this->session->userdata('cart');
    }

    $date = "%m";
    $month = mdate($date, time());
    $point_value = isset($this->Members_Model->get_points($member_id, $month)->point_value) ? $this->Members_Model->get_points($member_id, $month)->point_value : 0;


    $cart_count = 0;
    $carts = $this->Order_Model->get_all_items($group, $member_id);
    foreach ($carts as $key => $cart) {
        $cart_count += $cart->cart_quantity;
    }
 ?>
<nav class="navbar navbar-default navbar-fixed" style="background: url('<?=base_url()?>assets/img/Header-2.png');">
    <div class="container-fluid">
        <div class="navbar-header" style=" color: #fff !important;">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?=$page_module?></a>
        </div>

        <div class="collapse navbar-collapse">
           <ul class="nav navbar-nav navbar-left">
               <li>
                   <a href="<?=base_url()?>order/cart/">
                       <i class="fa fa-shopping-cart"></i>
                       <?php if (count($carts) != 0): ?>
                           <span class="notification"><?=$cart_count?></span>
                       <?php endif; ?>
                   </a>
               </li>
           </ul>
        <?php
            $id = $this->session->userdata('user')['vit_id'];
            $info = $this->Members_Model->check_member_id($id);
         ?>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?=$info->member_fname?> <?=$info->member_lname?>
                            <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu">
                        <li><Br/><center><?=$id?></center></li>
                        <li class="divider"></li>
                        <li><center>Points Accumulated : <?=$point_value;?></center></li>
                        <li class="divider"></li>
                        <li><a href="<?=base_url()?>account/">Account Details</a></li>
                        <li><a href="<?=base_url()?>income/">Income Details</a></li>
                      </ul>
                </li>
                <li>
                    <a href="<?=base_url()?>login/logout/">
                        Log out
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
