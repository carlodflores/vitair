<?php
	$this->load->view('header_view');
	$this->load->view('left_panel_view');
 ?>


    <div class="main-panel">
		<?php $this->load->view('top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Profile / <?=$member->vit_id?> / <?=$member->member_fname?> <?=$member->member_lname?></h4>
                            </div>
                            <div class="content">
								<form action="<?=base_url()?>account/" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Direct Referral ID <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" disabled name="txt_referral_id" placeholder="Direct Referral ID" value="<?=($this->Members_Model->get_member_info_id($member->member_referral_id) !== null)? $this->Members_Model->get_member_info_id($member->member_referral_id)->vit_id: ''?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">First Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=$member->member_fname?>" />
										<?=form_error('txt_fname', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Last Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_lname" placeholder="Last Name" value="<?=$member->member_lname?>" />
										<?=form_error('txt_lname', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">PDC ID<span class="text-danger">*</span></label>
        								<input type="text" class="form-control" readonly="readonly" name="txt_pdc_id" placeholder="Product Distribution Center ID" value="<?=$this->Franchise_Model->get_franchise_id($member->member_franchise_code)->franchiser_code?>" />
										<?=form_error('txt_pdc_id', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Activation code <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" readonly="readonly" name="txt_activation_code" placeholder="Activation code" value="<?=$member->member_activation_code?>"/>
										<?=form_error('txt_activation_code', '<br/><p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

								<hr>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="exampleInputEmail1">Sex</label>
											<select class="form-control" name="txt_sex">
												<option value="Male"<?= ($member->member_sex == 'Male' ? 'selected': '')?>>Male</option>
												<option value="Female" <?= ($member->member_sex == 'Female' ? 'selected': '')?>>Female</option>
											</select>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
										<label for="exampleInputEmail1">Civil Status</label>
										<select class="form-control" name="txt_civil">
											<option value="Single" <?= ($member->member_civil == 'Single' ? 'selected': '')?>>Single</option>
											<option value="Married" <?= ($member->member_civil == 'Married' ? 'selected': '')?>>Married</option>
											<option value="Separated" <?= ($member->member_civil == 'Separated' ? 'selected': '')?>>Separated</option>
											<option value="Widowed" <?= ($member->member_civil == 'Widowed' ? 'selected': '')?>>Widowed</option>
											<option value="Annulled" <?= ($member->member_civil == 'Annulled' ? 'selected': '')?>>Annulled</option>
										</select>
										</div>
									</div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Contact #</label>
            								<input type="text" class="form-control" name="txt_contact" placeholder="Contact #" value="<?=$member->member_contact?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
            								<input type="text" class="form-control" name="txt_email" placeholder="Email" value="<?=$member->member_email?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Date of Birth</label>
            								<input type="date" class="form-control" name="txt_birth" placeholder="Date of Birth" value="<?=$member->member_birthday?>" />
                                        </div>
                                    </div>

									<div class="col-md-10">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Address</label>
            								<input type="text" class="form-control" name="txt_address" placeholder="Address" value="<?=$member->member_address?>"/>
                                        </div>
                                    </div>

									<div class="col-md-2">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Zip Code</label>
            								<input type="text" class="form-control" name="txt_zip" placeholder="Zip Code" value="<?=$member->member_zipcode?>"/>
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">TIN</label>
        								<input type="text" class="form-control" name="txt_tin" placeholder="TIN" value="<?=$member->member_tin?>" />
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Occupation</label>
        								<input type="text" class="form-control" name="txt_occupation" placeholder="Occupation" value="<?=$member->member_occupation?>" />
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Company Name</label>
        								<input type="text" class="form-control" name="txt_company" placeholder="Company" value="<?=$member->member_company?>" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Name of Beneficiary</label>
        								<input type="text" class="form-control" name="txt_beneficiary" placeholder="Name of Beneficiary (Full Name)" value="<?=$member->member_beneficiary?>" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Relation</label>
        								<input type="text" class="form-control" name="txt_relation" placeholder="Relation to the Beneficiary" value="<?=$member->member_relation?>" />
                                        </div>
                                    </div>
                                </div>
								<hr>
								<h4 class="title">Change Password</h4>
								<div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Old Password</label>
        								<input type="text" class="form-control" name="txt_old_password" placeholder="Old Password" value="<?=$member->member_beneficiary?>" />
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">New Password</label>
        								<input type="text" class="form-control" name="txt_new_password" placeholder="New Password" value="<?=$member->member_relation?>" />
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Confirm New Password</label>
        								<input type="text" class="form-control" name="txt_confirm_password" placeholder="Confirm New Password" value="<?=$member->member_relation?>" />
                                        </div>
                                    </div>
                                </div>

                                <Br/>
								<button type="submit" class="btn btn-info btn-fill pull-right">Update</button>
								<div class="clearfix"></div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
<?php if ($notif !== ""): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "<?=$notif?>"

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
