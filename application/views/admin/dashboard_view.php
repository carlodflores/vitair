<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

	$id = $this->session->userdata('user')['member_id'];
	$this->Computation_Model->get_pairing($id);
 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="header">
								Pairing vs MLM
								<p class="category">Line Chart</p>
							</div>
							<div class="content">
								<div id="chartPerformance" class="ct-chart ct-perfect-fourth"></div>
								<div class="legend">
                                    <i class="fa fa-circle text-info"></i> MLM
                                    <i class="fa fa-circle text-danger"></i> Pairing
                                </div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>

		<?php
			$pairing = "";
			$label = "";
			$mlm = "";


			foreach ($this->Members_Model->chart_pairing() as $key => $value) {
				$pairing .= $value->pairing_amounts . ",";
				#$mlm .= $value->mlm_amount . ",";
				$label .= "'". date('M', strtotime($value->date_created)) . "',";
			}

			foreach ($this->Members_Model->chart_mlm() as $key => $value) {
				#$pairing .= $value->pairing_amount . ",";
				$mlm .= $value->mlm_amounts . ",";
				$label .= "'". date('M', strtotime($value->date_created)) . "',";
			}


			$mlm = substr($mlm, 0, -1);
			$pairing = substr($pairing, 0, -1);
			$label = substr($label, 0, -1);

			$label = explode(',', $label);
			$label = array_unique($label);

			$label = implode($label, ',');
		 ?>
<?php $this->load->view('footer_view'); ?>
<script type="text/javascript">
	$(function() {
	   var dataPerformance = {
		 labels: ["", <?=$label?>],
		 series: [
		   [0, <?=str_replace('.00', '', $mlm)?>],
			[0, <?=str_replace('.00', '', $pairing)?>]
		 ]
	   };

	   var optionsPerformance = {
		 showPoint: true,
		 lineSmooth: true,
		 height: "260px",
		 axisX: {
		   showGrid: false,
		   showLabel: true
		 }
	   };

	   Chartist.Line('#chartPerformance', dataPerformance, optionsPerformance);
	   document.getElementById('js-legend').innerHTML = myChart.generateLegend();

	});
</script>
