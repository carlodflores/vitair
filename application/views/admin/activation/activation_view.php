<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

	$id = $this->session->userdata('user')['member_id'];
	$this->Computation_Model->get_pairing($id);
 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-12">
                        <div class="card">
                            <div class="header">
								<div style="float :left;">
	                                <h4 class="title">Registration Pack</h4>
	                                <p class="category">Manage Registration Pack for head office and franchise.</p>
								</div>
								<a href="<?=base_url()?>admin/activation/generate/"><button class="btn btn-info btn-fill pull-right">Generate Activation</button></a>
								<Br/>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>#</th>
                                        <th data-field="code" data-sortable="true">Activation Code</th>
                                        <th data-field="realeased" data-sortable="true">Date Realased</th>
										<th data-field="to" data-sortable="true">Released To</th>
                                        <th data-field="status" data-sortable="true">Status</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($activation as $key => $value): ?>
											<?php
												$info = $this->Franchise_Model->get_franchise_id($value->franchiser_id);
											 ?>
											<tr>
	                                        	<td><?=$value->activation_id?></td>
	                                        	<td><?=$value->activation_code?></td>
	                                        	<td><?=$value->date_created?></td>
												<td><?=$info->franchiser_fname . ' ' . $info->franchiser_lname . '<br/><small>' . $info->franchiser_code . '</small>'?></td>
	                                            <td>
													<?php if ($value->activation_status == 0): ?>
														Unavailable (<?=$this->Members_Model->get_member_activation($value->activation_code)->vit_id?>)
													<?php else: ?>
														Available
													<?php endif; ?>
												</td>
	                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('footer_view'); ?>

<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>
