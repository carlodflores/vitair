<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

 ?>

<div class="main-panel">
	<?php $this->load->view('admin/top_nav_view'); ?>

    <div class="content">
		<br/><br/><br/>
        <div class="container-fluid">
            <div class="row">
				<div class="col-md-10 col-md-offset-1">
                    <div class="card">
                        <div class="header">
                            <div style="float :left;">
								<h4 class="title">List of all Products</h4>
								<p class="category">All the products that the company cater.</p>
							</div>
							<a href="<?=base_url()?>admin/product/add/"><button class="btn btn-info btn-fill pull-right">Add Product</button></a>
                        </div>
						<div class="clearfix"></div>
						<div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped sortid">
								<thead>
									<tr>
										<th></th>
										<th data-field="name" data-sortable="true">Product Name</th>
										<th data-field="price" data-sortable="true">Price</th>
										<th data-field="points" data-sortable="true">Reward Points</th>
                                        <th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($products as $key => $value): ?>
                                        <tr>
                                            <td></td>
                                            <td><?=$value->product_name?></td>
                                            <td><?=$value->product_price?></td>
                                            <td><?=$value->product_point_value?></td>
                                            <td>
                                                <a href="<?=base_url()?>admin/product/edit/<?=$value->product_id?>/" title="Restock" class="btn btn-success btn-simple btn-xs">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </td>
                                        </tr>
									<?php endforeach; ?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('footer_view'); ?>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>


<?php if (isset($_GET['success']) && $_GET['success'] == md5('edit')): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully Editted a Product."

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>


<?php if (isset($_GET['success']) && $_GET['success'] == md5('add')): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully Added a Product."

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
