<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-8 col-md-offset-2">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Add Product Distribution Centers Location</h4>
                            </div>
                            <div class="content">
                                <form role="form" action="<?=base_url()?>admin/pdc/location/add/" method="post">
								<div class="row">
									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Location <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_location_name" placeholder="Location Name (Ex. Santolan Pasig City)" value="<?=set_value('txt_location_name')?>" /><br/>
										<?=form_error('txt_location_name', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Location Code <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_location_code" placeholder="Location Code (Ex. SPC)" value="<?=set_value('txt_location_code')?>" /><br/>
										<?=form_error('txt_location_code', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-info btn-fill pull-right">Save</button>
										<div class="clearfix"></div>
									</div>
								</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('footer_view'); ?>
<?php if (isset($_GET['success'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully generated a new Activation Code"

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
