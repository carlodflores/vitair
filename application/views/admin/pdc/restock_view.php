<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

 ?>

    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-8 col-md-offset-2">
                        <div class="card">
                            <div class="header">
								<div style="float: left;">
									<h4 class="title">Restock Product Distribution Centers</h4>
								</div>
								<button type="button" id="add_field" class="btn btn-info btn-fill pull-right"><i class="fa fa-plus"> </i></button>
								<div class="clearfix"></div>
                            </div>
                            <div class="content">
                                <form role="form" action="<?=base_url()?>admin/pdc/restock/<?=$pdc_id?>/" method="post">

								<div class="loader">
									<div class="row" id="field_input">
										<div class="col-md-6">
	                                        <div class="form-group">
	                                        <label for="exampleInputEmail1">Product <span class="text-danger">*</span></label>
	        								<select type="text" class="form-control" name="txt_product[]" placeholder="Product" value="<?=set_value('txt_product')?>">
	                                            <?php foreach ($this->Product_Model->get_all_products() as $key => $value): ?>
	                                                <option <?=set_select('txt_product', $value->product_id, TRUE); ?> value="<?=$value->product_id?>"><?=$value->product_name?></option>
	                                            <?php endforeach; ?>
	                                        </select>
	                                        <br/>
											<?=form_error('txt_product', '<p class="text-danger">', '</p>')?>
	                                        </div>
	                                    </div>

										<div class="col-md-6">
	                                        <div class="form-group">
	                                        <label for="exampleInputEmail1">Quantity <span class="text-danger">*</span></label>
	        								<input type="number" min="1" class="form-control" name="txt_quantity[]" placeholder="Quantity" value="<?=set_value('txt_quantity')?>" /><br/>
											<?=form_error('txt_quantity', '<p class="text-danger">', '</p>')?>
	                                        </div>
	                                    </div>
	                                </div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-info btn-fill pull-right">Save</button>
										<div class="clearfix"></div>
									</div>
								</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/><br/><br/><br/>

<?php $this->load->view('footer_view'); ?>
<script type="text/javascript">
	$(function() {
		$("#add_field").click(function() {
			$("#field_input").clone().appendTo('.loader');
		});
	});
</script>
<?php if (isset($_GET['success'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully generated a new Activation Code"

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
