<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');

 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <div style="float :left;">
                                    <h4 class="title">Product Distribution Centers</h4>
                                    <p class="category">List of all the Registered Product Distribution Centers</p>
                                </div>
                                <a href="<?=base_url()?>admin/pdc/location/add/"><button class="btn btn-info btn-fill pull-right">Add Location Code</button></a>
                                <Br/>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>Location ID</th>
                                        <th>Short Code</th>
                                        <th>Location</th>
										<th>Date Registered</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($locations as $key => $value): ?>
                                            <tr>
                                                <td><?=$value->location_id?></td>
                                                <td><?=$value->location_short_code?></td>
                                                <td><?=$value->location_name?></td>
                                                <td><?=$value->date_created?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $this->load->view('footer_view'); ?>

<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>
