<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');
 ?>

<div class="main-panel">
	<?php $this->load->view('admin/top_nav_view'); ?>

    <div class="content">
		<br/><br/><br/>
        <div class="container-fluid">
            <div class="row">
				<div class="col-md-10 col-md-offset-1">
                    <div class="card">
                        <div class="header">
                            <div style="float :left;">
								<h4 class="title">Pending Payout Of <?=$this->Members_Model->get_member_info_id($member_id)->vit_id?></h4>
								<p class="category">Payout History.</p>
							</div>
                        </div>
						<div class="clearfix"></div>
						<div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped sortid">
								<thead>
									<tr>
										<th data-field="name" data-sortable="true">Date Generated</th>
										<th data-field="id" data-sortable="true">Payout Amount</th>
										<th data-field="refer" data-sortable="true">Payout Status</th>
                                        <th>Action</th>
									</tr>
								</thead>
								<tbody>
                                    <?php foreach ($this->Members_Model->get_all_payouts($member_id) as $key => $value): ?>
                                        <td><?=$value->date_created?></td>
                                        <td><?=$value->payout_amount?></td>
                                        <td>
                                            <?php if ($value->payout_status == 1): ?>
                                                Unclaimed
                                            <?php else: ?>
                                                Claimed
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if ($value->payout_status == 1): ?>
                                                <a href="<?=base_url()?>admin/account/pay/<?=$value->member_id?>/<?=$value->payout_id?>/" title="Cash Out" class="btn btn-info btn-simple btn-xs">
                                                    <i title="Cash Out" class="pe-7s-cash" style="font-size: 18px;"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    <?php endforeach; ?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('footer_view'); ?>
<script type="text/javascript">
    // Create the tooltips only when document ready
$(document).ready(function () {

// This will automatically grab the 'title' attribute and replace
// the regular browser tooltips for all <a> elements with a title attribute!
$('a[title]').qtip();

});
    </script>

<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>
