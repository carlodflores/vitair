<?php
	$this->load->view('admin/header_view');
	$this->load->view('admin/left_panel_view');
 ?>


    <div class="main-panel">
		<?php $this->load->view('admin/top_nav_view'); ?>

        <div class="content">
			<br/><br/><br/>
            <div class="container-fluid">
                <div class="row">
					<div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Register Account</h4>
                            </div>
                            <div class="content">
								<form class="" action="<?=base_url()?>admin/account/" method="post">
								<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">First Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_fname" placeholder="First Name" value="<?=set_value('txt_fname')?>" /><br/>
										<?=form_error('txt_fname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Last Name <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_lname" placeholder="Last Name" value="<?=set_value('txt_lname')?>" /><br/>
										<?=form_error('txt_lname', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">PDC ID<span class="text-danger">*</span></label>
										<select class="form-control" name="txt_pcd_id">
											<?php foreach ($this->Franchise_Model->get_all_franchisers() as $key => $value): ?>
												<option value="<?=$value->franchiser_code?>"><?=$value->franchiser_code?></option>
											<?php endforeach; ?>
										</select>
										<?=form_error('txt_pcd_id', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Activation code <span class="text-danger">*</span></label>
        								<input type="text" class="form-control" name="txt_activation_code" placeholder="Activation code" value="<?=set_value('txt_activation_code')?>"/><br/>
										<?=form_error('txt_activation_code', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Password <span class="text-danger">*</span></label>
        								<input type="password" class="form-control" name="txt_password" placeholder="Password" value="<?=set_value('txt_password')?>" /><br/>
										<?=form_error('txt_password', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>

									<div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Confirm Password <span class="text-danger">*</span></label>
        								<input type="password" class="form-control" name="txt_re_password" placeholder="Confirm Password" value="<?=set_value('txt_re_password')?>" /><br/>
										<?=form_error('txt_re_password', '<p class="text-danger">', '</p>')?>
                                        </div>
                                    </div>
                                </div>


								<hr>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="exampleInputEmail1">Sex</label>
											<select class="form-control" name="txt_sex">
												<option value="Male" <?= set_select('txt_sex', 'Male', TRUE)?>>Male</option>
												<option value="Female" <?= set_select('txt_sex', 'Female', TRUE)?>>Female</option>
											</select>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
										<label for="exampleInputEmail1">Civil Status</label>
										<select class="form-control" name="txt_civil">
											<option value="Single" <?= set_select('txt_civil', 'Single', TRUE)?>>Single</option>
											<option value="Married" <?= set_select('txt_civil', 'Married', TRUE)?>>Married</option>
											<option value="Separated" <?= set_select('txt_civil', 'Separated', TRUE)?>>Separated</option>
											<option value="Widowed" <?= set_select('txt_civil', 'Widowed', TRUE)?>>Widowed</option>
											<option value="Annulled" <?= set_select('txt_civil', 'Annulled', TRUE)?>>Annulled</option>
										</select>
										</div>
									</div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Contact #</label>
            								<input type="text" class="form-control" name="txt_contact" placeholder="Contact #" value="<?=set_value('txt_contact')?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
            								<input type="text" class="form-control" name="txt_email" placeholder="Email" value="<?=set_value('txt_email')?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Date of Birth</label>
            								<input type="date" class="form-control" name="txt_birth" placeholder="Date of Birth" value="<?=set_value('txt_birth')?>" />
                                        </div>
                                    </div>

									<div class="col-md-10">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Address</label>
            								<input type="text" class="form-control" name="txt_address" placeholder="Address" value="<?=set_value('txt_address')?>"/>
                                        </div>
                                    </div>

									<div class="col-md-2">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Zip Code</label>
            								<input type="text" class="form-control" name="txt_zip" placeholder="Zip Code" value="<?=set_value('txt_zip')?>"/>
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">TIN</label>
        								<input type="text" class="form-control" name="txt_tin" placeholder="TIN" value="<?=set_value('txt_tin')?>" />
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Occupation</label>
        								<input type="text" class="form-control" name="txt_occupation" placeholder="Occupation" value="<?=set_value('txt_occupation')?>" />
                                        </div>
                                    </div>

									<div class="col-md-4">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Company Name</label>
        								<input type="text" class="form-control" name="txt_company" placeholder="Company" value="<?=set_value('txt_company')?>" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Name of Beneficiary</label>
        								<input type="text" class="form-control" name="txt_beneficiary" placeholder="Name of Beneficiary (Full Name)" value="<?=set_value('txt_beneficiary')?>" />
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label for="exampleInputEmail1">Relation</label>
        								<input type="text" class="form-control" name="txt_relation" placeholder="Relation to the Beneficiary" value="<?=set_value('txt_relation')?>" />
                                        </div>
                                    </div>
                                </div>
								<hr>
                                <Br/>
								<button type="submit" class="btn btn-info btn-fill pull-right">Save</button>
								<div class="clearfix"></div>
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $this->load->view('footer_view'); ?>
<?php if (isset($_GET['success'])): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully added a member"

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
