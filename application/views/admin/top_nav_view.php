
<nav class="navbar navbar-default navbar-fixed" style="background: url('<?=base_url()?>assets/img/Header-2.png');">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><?=$page_module?></a>
        </div>
        <?php
            $id = $this->session->userdata('admin')['admin_code'];
            $info = $this->Admin_Model->get_admin_by_id($id);
         ?>
        <div>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?=$info->admin_fname?>  <?=$info->admin_lname?>
                            <b class="caret"></b>
                      </a>
                      <ul class="dropdown-menu">
                        <li><Br/><center><a href="<?=base_url()?>admin/account/profile/"><?=$id?></a></center></li>
                        <li class="divider"></li>
                        <li><a href="<?=base_url()?>admin/account/profile/">Account Details</a></li>
                      </ul>
                </li>
                <li>
                    <a href="<?=base_url()?>admin/login/logout/">
                        Log out
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
