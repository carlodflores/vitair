<?php
	$this->load->view('header_view');
	$this->load->view('left_panel_view');

    $root = $id;
    if($this->uri->segment(3) !== null) {
        $id = $this->uri->segment(3);
    }

    $this->Computation_Model->get_pairing($id);
	$this->Computation_Model->get_pairing_count($id);
 ?>
 <style media="screen">
 	table tr td:nth-child(2) {
 		padding-left: 10px;
 	}
 </style>

<div class="main-panel">
	<?php $this->load->view('top_nav_view'); ?>

    <div class="content">
		<br/><br/><br/>
        <div class="container-fluid" style="max-width: auto;">
            <div class="row">
                <?php if ($this->uri->segment(3) !== null): ?>
                    <div style="float:left;">
                        <a href="<?=base_url() . 'binary/graph/'?>"><button class="btn btn-info btn-fill"><i class="pe-7s-user user-display"></i><br/>Back To Head</button></a>
                    </div>
                <?php endif; ?>
                <div style="float:left; margin-left: 20px;">
                    <table>
                        <tr>
                            <td>Name: </td>
                            <td><?=$this->Members_Model->get_member_info_id($id)->member_fname . ' ' . $this->Members_Model->get_member_info_id($id)->member_lname ?></td>
                        </tr>

                        <tr>
                            <td>Account ID:</td>
                            <td> <?=$this->Members_Model->get_member_info_id($id)->vit_id?></td>
                        </tr>

                        <tr>
                            <td>Date Registered: </td>
                            <td><?=$this->Members_Model->get_member_info_id($id)->date_created?></td>
                        </tr>

                        <!--
						<tr>
                            <td>Total Left: </td>
                            <td><?=$this->Computation_Model->get_pos('L')?></td>
                        </tr>

                        <tr>
                            <td>Total Right: </td>
                            <td><?=$this->Computation_Model->get_pos('R')?></td>
                        </tr>
						-->
                    </table>
                </div>
            </div>
            <div class="row">
				<div class="col-md-12">
                    <div class="tree">
                        <ul>
                            <li>

                                <a href="#"><img src="" alt="" /><i class="pe-7s-user user-display"></i><br/><?=$this->Members_Model->get_member_info_id($id)->member_fname . ' ' . $this->Members_Model->get_member_info_id($id)->member_lname ?>
								<br/><?=$this->Members_Model->get_member_info_id($id)->vit_id ?></a>
                                <ul>
									<?php $result =  $this->Members_Model->get_member_downline($id); ?>
									<?php if (count($result) == 1 && $result[0]->relation_position == "R" || count($result) <= 0): ?>
										<li>
			                                <a href="#"><BR/><i class="pe-7s-close-circle user-display"></i>
											<br/><br/></a>
										</li>
									<?php endif; ?>
									<?php foreach ($result as $key => $row1): ?>
										<?php $info = $this->Members_Model->get_member_info_id($row1->relation_child); ?>
										<li>
			                                <a href="<?=base_url()?>binary/graph/<?=$info->member_id?>"><i class="pe-7s-user user-display"></i><br/><?=$info->member_fname . ' ' . $info->member_lname ?>
											<br/><?=$info->vit_id ?></a>
			                                <ul>
												<!-- Level 2 Start -->
												<?php $result2 =  $this->Members_Model->get_member_downline($row1->relation_child); ?>
												<?php if (count($result2) == 1 && $result2[0]->relation_position == "R" || count($result2) <= 0): ?>
													<li>
														<a href="#"><BR/><i class="pe-7s-close-circle user-display"></i>
														<br/><br/></a>
													</li>
												<?php endif; ?>
												<?php foreach ($result2 as $key => $row2): ?>
													<?php $info = $this->Members_Model->get_member_info_id($row2->relation_child); ?>
													<li>
														<a href="<?=base_url()?>binary/graph/<?=$info->member_id?>"><i class="pe-7s-user user-display"></i><br/><?=$info->member_fname . ' ' . $info->member_lname ?>
														<br/><?=$info->vit_id ?></a>
														<ul>
															<!-- Level 3 Start -->
															<?php $result3 =  $this->Members_Model->get_member_downline($row2->relation_child); ?>
															<?php if (count($result3) == 1 && $result3[0]->relation_position == "R" || count($result3) <= 0): ?>
																<li>
																	<a href="#"><BR/><i class="pe-7s-close-circle user-display"></i>
																	<br/><br/></a>
																</li>
															<?php endif; ?>
															<?php foreach ($result3 as $key => $row3): ?>
																<?php $info = $this->Members_Model->get_member_info_id($row3->relation_child); ?>
																<li>
																	<a href="<?=base_url()?>binary/graph/<?=$info->member_id?>"><i class="pe-7s-user user-display"></i><br/><?=$info->member_fname . ' ' . $info->member_lname ?>
																	<br/><?=$info->vit_id ?></a>
																</li>
															<?php endforeach; ?>
															<?php if (count($result3) == 1 && $result3[0]->relation_position == "L" || count($result3) <= 0): ?>
																<li>
																	<a href="#"><BR/><i class="pe-7s-close-circle user-display"></i>
																	<br/><br/></a>
																</li>
															<?php endif; ?>
															<!-- Level 3 End -->
														</ul>
													</li>
												<?php endforeach; ?>
												<?php if (count($result2) == 1 && $result2[0]->relation_position == "L" || count($result2) <= 0): ?>
													<li>
														<a href="#"><BR/><i class="pe-7s-close-circle user-display"></i>
														<br/><br/></a>
													</li>
												<?php endif; ?>
												<!-- Level 2 End -->
			                                </ul>
			                            </li>
									<?php endforeach; ?>
									<?php if (count($result) == 1 && $result[0]->relation_position == "L" || count($result) <= 0): ?>
										<li>
			                                <a href="#"><BR/><i class="pe-7s-close-circle user-display"></i>
											<br/><br/></a>
										</li>
									<?php endif; ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('footer_view'); ?>
<link rel="stylesheet" href="<?=base_url()?>assets/css/tree.css" media="screen" title="no title" charset="utf-8">
