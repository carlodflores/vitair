<?php
	$this->load->view('header_view');
	$this->load->view('left_panel_view');


	$level = ($this->Members_Model->get_member_level($id) !== null)? $this->Members_Model->get_member_level($id)->relation_ancestor : '';
	$level = (count(explode('/', $level)));
 ?>

<div class="main-panel">
	<?php $this->load->view('top_nav_view'); ?>

    <div class="content">
		<br/><br/><br/>
        <div class="container-fluid">
            <div class="row">
				<div class="col-md-10 col-md-offset-1">
                    <div class="card">
                        <div class="header">
                            <div style="float :left;">
								<h4 class="title">Tabular Binary Details</h4>
								<p class="category">The tabular display of you binary system.</p>
							</div>
							<a href="<?=base_url()?>binary/graph/"><button class="btn btn-info btn-fill pull-right">View Graphical Display</button></a>
                        </div>
						<div class="clearfix"></div>
						<div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped sortid" data-sort-name="level" data-sort-order="asc">
								<thead>
									<tr>
										<th data-field="level" data-sortable="true">Level</th>
										<th data-field="name" data-sortable="true">Member Name</th>
										<th data-field="id" data-sortable="true">Member ID</th>
										<th data-field="points" data-sortable="true">Points</th>
										<th data-field="refer" data-sortable="true">Direct Referral</th>
										<th data-field="date" data-sortable="true">Registration Date</th>
									</tr>
								</thead>
								<tbody>
									<?=$this->Computation_Model->get_tabular_view($id, $level)?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('footer_view'); ?>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>
