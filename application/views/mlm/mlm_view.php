<?php
	$this->load->view('header_view');
	$this->load->view('left_panel_view');


    $member_id = $this->session->userdata('user')['member_id'];

	$level = ($this->Members_Model->get_member_level($member_id) !== null)? $this->Members_Model->get_member_level($member_id)->relation_ancestor : '';
	$level = (count(explode('/', $level)));
    $this->Computation_Model->mlm_bonus($member_id, $level);

	$date = "%m";
	$month = mdate($date, time());
	$point_value = isset($this->Members_Model->get_points($member_id, $month)->point_value) ? $this->Members_Model->get_points($member_id, $month)->point_value : 0;
	$level_points = $this->Computation_Model->get_level_points();
	$members = $this->Computation_Model->get_level_arr_member();
	$puntos = $this->Computation_Model->get_level_arr_pointsa();

	$or_level = $level;

    $arr = $this->Computation_Model->get_level_arr();
    sort($arr, SORT_NUMERIC);

	#print_r($level_points);
    $counter = array_count_values($arr);
 ?>

<div class="main-panel">
	<?php $this->load->view('top_nav_view'); ?>

    <div class="content">
		<br/><br/><br/>
        <div class="container-fluid">
            <div class="row">
				<div class="col-md-10 col-md-offset-1">
                    <div class="card">
                        <div class="header">
							<h4 class="title">MLM Details</h4>
							<p class="category">The tabular display of your mlm system.</p>
                        </div>
						<div class="clearfix"></div>
						<div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped sortid">
								<thead>
									<tr>
										<th data-field="level" data-sortable="true">Level</th>
										<th data-field="name" data-sortable="true">Equation</th>
										<th data-field="date" data-sortable="true">Profit</th>
									</tr>
								</thead>
								<tbody>
                                    <tr>
                                        <td>U</td>
                                        <td>5% x <?=$point_value?> (Total Points)</td>
                                        <td>₱ <?=number_format(1*0.05*$point_value, 2) ?> </td>
                                    </tr>
									<?php
										$overall = 1*0.05*$point_value;
                                        foreach ($counter as $key => $value) {
											$total = 0;
											$points = 0;

                                        echo "<tr>";
                                            $key = ($key-$or_level);
                                            $percentage = 5;
                                            switch ($key) {
                                                case '1': $percentage = 5; break;
                                                case '2': $percentage = 4; break;
                                                case '3': $percentage = 3; break;
                                                case '4': $percentage = 2; break;
                                                default: $percentage = 1; break;
                                            }

                                            if(($key) <= 15) {

												foreach ($members as $member_id => $level) {
													if(($level-$or_level) == $key) {
														$total += (($percentage/100)*$puntos[$member_id]);
														$points += $puntos[$member_id];
														unset($members[$member_id]);
													}
												}

												$overall += $total;

                                    ?>
                                                <td><?=$key?></td>
                                                <td><?=$percentage . "% x " . $points?> (Total Points)</td>
                                                <td>₱ <?=number_format($total, 2)?></td>
                                    <?php
                                            }
                                            echo "</tr>";
                                        }
                                     ?>
									 <tr>
                                         <td></td>
                                         <td><b>Total</b></td>
                                         <td><b>₱ <?=number_format($overall, 2) ?> </b></td>
                                     </tr>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('footer_view'); ?>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: false,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>
