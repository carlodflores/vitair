<?php
	$this->load->view('header_view');
 ?>
<section style="background: url('<?=base_url()?>assets/img/sidebar-5.jpg') no-repeat; background-size: cover; width: 100vw; height: 100vh;">
	<div class="container-fluid">
		<br/><br/>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="card">
					<div class="header">
						<div>
							<center>
								<BR/>
								<h4 class="title">Login</h4>
							</center>
							<?php  if(isset($login_error)): ?>
								<br/>
								<div class="alert alert-danger">
									<span><b> Danger - </b> This is a regular notification made with ".alert-danger"</span>
								</div>
							<?php endif; ?>
							<div class="content">
								<form action="<?=base_url()?>login/" method="post">
									<div class="form-group">
										<label for="memberidInput">Member ID</label>
										<input type="text" name="txt_member_id" class="form-control" placeholder="Member ID">
										<?php echo form_error('txt_member_id', '<span class="text-danger">', '</span>'); ?>
									</div>

									<div class="form-group">
										<label for="memberpassInput">Password</label>
										<input type="password" name="txt_member_password" class="form-control" placeholder="Password">
										<?php echo form_error('txt_member_password', '<span class="text-danger">', '</span>'); ?>
									</div>

									<div class="form-group" style="float: left;">
										<p>
											Forgot Password?<Br/><a href="<?=base_url()?>login/password/">Click Here</a>
										</p>
									</div>
									<button type="submit" class="btn btn-info btn-fill pull-right" style="margin-left: 10px;">Login</button>
									<a href="<?=base_url()?>register/">
										<button type="button" class="btn btn-info btn-fill pull-right">Register</button>
									</a>
									<div class="clearfix"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php if (isset($_GET['success']) && $_GET['success'] == md5('registration')): ?>
	<script>
		$(function() {
			$.notify({
				icon: 'pe-7s-check',
				message: "You've successfully Registered as a member."

			},{
				type: 'success',
				timer: 4000
			});
		});
	</script>
<?php endif; ?>
