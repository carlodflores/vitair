<body>

<div class="wrapper">
    <div class="sidebar" data-color="transparent" data-image="<?=base_url()?>assets/img/Sidebar.png">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="logo-text">
                    <img src="<?=base_url()?>assets/img/Logo-2.png" width="200" alt="" />
                </a>
            </div>


            <ul class="nav">
                <li <?=($this->uri->segment(2) == "dashboard")?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>pdc/dashboard/">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(3), "members") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>pdc/account/members/">
                        <i class="pe-7s-user"></i>
                        <p>Members</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(2), "activation") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>pdc/activation/">
                        <i class="pe-7s-cash"></i>
                        <p>Registration Pack</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(3), "all") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>pdc/order/all/">
                        <i class="pe-7s-note2"></i>
                        <p>Member Orders</p>
                    </a>
                </li>

                <li <?=(strcasecmp($this->uri->segment(3), "stocks") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>pdc/order/stocks">
                        <i class="pe-7s-note2"></i>
                        <p>Stocks</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(3), "reports") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>pdc/account/reports/">
                        <i class="pe-7s-link"></i>
                        <p>Payout Reports</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
