<?php
	$this->load->view('pdc/header_view');
	$this->load->view('pdc/left_panel_view');

 ?>

<div class="main-panel">
	<?php $this->load->view('pdc/top_nav_view'); ?>

    <div class="content">
		<br/><br/><br/>
        <div class="container-fluid">
            <div class="row">
				<div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <div style="float :left;">
								<h4 class="title">Distributed Member Payout</h4>
								<p class="category">List of Claimed Payouts</p>
							</div>
                        </div>
						<div class="clearfix"></div>
						<div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped sortid">
								<thead>
									<tr>
										<th data-field="name" data-sortable="true">Member Name</th>
										<th data-field="id" data-sortable="true">Member ID</th>
										<th data-field="date" data-sortable="true">Amount</th>
									</tr>
								</thead>
								<tbody>
                                    <?php foreach ($this->Members_Model->get_all_payouts_franchise($this->session->userdata('franchise')['franchise_id']) as $key => $value): ?>
										<tr>
                                            <td><?=$value->member_fname . ' ' . $value->member_lname?></td>
                                            <td><?=$value->vit_id?></td>
                                            <td>₱ <?=number_format($value->payout_amount, 2)?></td>
                                        </tr>
                                    <?php endforeach; ?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>

				<div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <div style="float :left;">
								<h4 class="title">Receive Payouts</h4>
								<p class="category">List of Receive Payouts</p>
							</div>
                        </div>
						<div class="clearfix"></div>
						<div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped sortid">
								<thead>
									<tr>
										<th data-field="date" data-sortable="true">Date Created</th>
										<th data-field="payout" data-sortable="true">Payout Amount</th>
										<th data-field="status" data-sortable="true">Status</th>
									</tr>
								</thead>
								<tbody>
                                    <?php foreach ($this->Members_Model->get_all_payouts_franchise_receive($this->session->userdata('franchise')['franchise_id']) as $key => $value): ?>
										<tr>
                                            <td><?=$value->date_created?></td>
                                            <td>P <?=number_format($value->total_amount, 2)?></td>
                                            <td>Received</td>
                                        </tr>
                                    <?php endforeach; ?>
								</tbody>
							</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('footer_view'); ?>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>
<script src="<?=base_url()?>assets/js/tableExport.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('table').bootstrapTable({
                toolbar: ".toolbar",
                clickToSelect: true,
                showRefresh: true,
                search: true,
                showToggle: true,
                showColumns: true,
                pagination: true,
                searchAlign: 'left',
                pageSize: 8,
                clickToSelect: false,
                pageList: [8,10,25,50,100],
				showExport: true,
                formatShowingRows: function(pageFrom, pageTo, totalRows){
                    //do nothing here, we don't want to show the text "showing x of y from..."
                },
                formatRecordsPerPage: function(pageNumber){
                    return pageNumber + " rows visible";
                },
                icons: {
                    refresh: 'fa fa-refresh',
                    toggle: 'fa fa-th-list',
                    columns: 'fa fa-columns',
                    detailOpen: 'fa fa-plus-circle',
                    detailClose: 'fa fa-minus-circle'
                }
            });
} );
</script>
