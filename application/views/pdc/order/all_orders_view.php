<?php

	$this->load->view('pdc/header_view');

	$this->load->view('pdc/left_panel_view');

 ?>





    <div class="main-panel">

		<?php $this->load->view('pdc/top_nav_view'); ?>



        <div class="content">

			<br/><br/><br/>

            <div class="container-fluid">

                <div class="row">

					<div class="col-md-12">

						<div class="card">

							<div class="header">

								<div style="float :left;">

	                                <h4 class="title">

										<?php if ($this->uri->segment(3) != 'done'): ?>

											Member Orders (Pending)

										<?php else: ?>

											Member Orders (Done)

										<?php endif; ?>

									</h4>

	                                <p class="category">Your Members Order and its Status</p>

								</div>

								<?php if ($this->uri->segment(3) != 'done'): ?>

									<a href="<?=base_url()?>pdc/order/done/"><button class="btn btn-info btn-fill pull-right">View Completed Orders</button></a>

								<?php else: ?>

									<a href="<?=base_url()?>pdc/order/"><button class="btn btn-info btn-fill pull-right">View Pending Orders</button></a>

								<?php endif; ?>

								<div class="clearfix"></div>

                            </div>

                            <div class="content table-responsive table-full-width">

                                <table class="table table-hover table-striped sortid">

    								<thead>

    									<tr>

    										<th data-field="id" data-sortable="true">Order #</th>

    										<th data-field="name" data-sortable="true">Member Name</th>

                                            <th data-field="price" data-sortable="true">Total Price</th>

                                            <th data-field="points" data-sortable="true">Total Points</th>

    										<th data-field="date" data-sortable="true">Date Created</th>

    										<th data-field="status" data-sortable="true">Status</th>

                                            <th data-field="action" data-sortable="true">Action</th>

    									</tr>

    								</thead>

    								<tbody>

                                        <?php foreach ($orders as $key => $value): ?>

                                            <tr>

												<?php

													$mi = $this->Members_Model->get_member_info_id($value->member_id);

												 ?>

												<td style="text-align: center; ">

	                                                <center>

	                                                    <a href="<?=base_url()?>pdc/order/info/<?=$value->order_id?>/">

	                                                        #<?=$value->order_id?>

	                                                    </a>

	                                                </center>

	                                            </td>

	                                            <td>

													<?=$mi->member_fname . ' ' , $mi->member_lname?><br/>

													<small><?=$mi->vit_id?></small>

												</td>

	                                            <td>P <?=number_format($value->order_total, 2)?></td>

	                                            <td><?=$value->order_total_points?></td>

	                                            <td><?=$value->date_created?></td>

	                                            <td><?=$value->order_status == 1 ? 'Pending' : 'Done'?></td>

                                                <td>

                                                <a href="<?=base_url()?>pdc/order/delete/<?=$value->order_id?>/" title="Delete Order" class="btn btn-info btn-simple btn-xs">

                                                    <i class="fa fa-times" style="font-size: 18px;"></i>

                                                </a>

                                            </td>

											</tr>

                                        <?php endforeach; ?>

    								</tbody>

    							</table>

                            </div>

						</div>

					</div>



                </div>

            </div>

        </div>

<?php $this->load->view('footer_view'); ?>

<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>

<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>

<script src="<?=base_url()?>assets/js/tableExport.js"></script>

<script type="text/javascript">

$(document).ready(function() {

    $('table').bootstrapTable({

                toolbar: ".toolbar",

                clickToSelect: true,

                showRefresh: true,

                search: true,

                showToggle: true,

                showColumns: true,

                pagination: true,

                searchAlign: 'left',

                pageSize: 8,

                clickToSelect: false,

                pageList: [8,10,25,50,100],

				showExport: true,

                formatShowingRows: function(pageFrom, pageTo, totalRows){

                    //do nothing here, we don't want to show the text "showing x of y from..."

                },

                formatRecordsPerPage: function(pageNumber){

                    return pageNumber + " rows visible";

                },

                icons: {

                    refresh: 'fa fa-refresh',

                    toggle: 'fa fa-th-list',

                    columns: 'fa fa-columns',

                    detailOpen: 'fa fa-plus-circle',

                    detailClose: 'fa fa-minus-circle'

                }

            });

        $(".btn.btn-info.btn-simple.btn-xs").qtip();


} );

</script>

