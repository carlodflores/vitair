<?php

	$this->load->view('pdc/header_view');

	$this->load->view('pdc/left_panel_view');

 ?>





    <div class="main-panel">

		<?php $this->load->view('pdc/top_nav_view'); ?>



        <div class="content">

			<br/><br/><br/>

            <div class="container-fluid">

                <div class="row">

					<div class="col-md-12">

						<div class="card">

							<div class="header">

								<div style="float :left;">

	                                <h4 class="title">
											Stocks
									</h4>

	                                <p class="category">Product Stocks</p>

								</div>

								<div class="clearfix"></div>

                            </div>

                            <div class="content table-responsive table-full-width">

                                <table class="table table-hover table-striped sortid">

    								<thead>

    									<tr>

    										<th data-field="id" data-sortable="true">Product Name</th>
											<th data-field="stock" data-sortable="true">Stock Quantity</th>
    										<th data-field="price" data-sortable="true">Product Price</th>
											<th data-field="worth" data-sortable="true">Worth</th>
    									</tr>

    								</thead>

    								<tbody>

                                        <?php foreach ($this->Order_Model->get_stock_info_one($this->session->userdata('franchise')['franchise_id']) as $key => $value): ?>

                                            <tr>

												<?php

													$pi = $this->Product_Model->get_product_details($value->product_id);
												 ?>

												<td style="text-align: center; ">

	                                                <center>
														<?= $pi->product_name  ?>
	                                                </center>

	                                            </td>

												<td>
													<?=$value->stock_value?>
												</td>

												<td>
													P <?=number_format($pi->product_price, 2)?>

												</td>

												<td>
													P <?= number_format($pi->product_price * $value->stock_value, 2) ?>

												</td>

											</tr>

                                        <?php endforeach; ?>

    								</tbody>

    							</table>

                            </div>

						</div>

					</div>



                </div>

            </div>

        </div>

<?php $this->load->view('footer_view'); ?>

<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>

<script src="<?=base_url()?>assets/js/bootstrap-table-export.js"></script>

<script src="<?=base_url()?>assets/js/tableExport.js"></script>

<script type="text/javascript">

$(document).ready(function() {

    $('table').bootstrapTable({

                toolbar: ".toolbar",

                clickToSelect: true,

                showRefresh: true,

                search: true,

                showToggle: true,

                showColumns: true,

                pagination: true,

                searchAlign: 'left',

                pageSize: 8,

                clickToSelect: false,

                pageList: [8,10,25,50,100],

				showExport: true,

                formatShowingRows: function(pageFrom, pageTo, totalRows){

                    //do nothing here, we don't want to show the text "showing x of y from..."

                },

                formatRecordsPerPage: function(pageNumber){

                    return pageNumber + " rows visible";

                },

                icons: {

                    refresh: 'fa fa-refresh',

                    toggle: 'fa fa-th-list',

                    columns: 'fa fa-columns',

                    detailOpen: 'fa fa-plus-circle',

                    detailClose: 'fa fa-minus-circle'

                }

            });

        $(".btn.btn-info.btn-simple.btn-xs").qtip();


} );

</script>
