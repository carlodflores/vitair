<body>

<div class="wrapper">
    <div class="sidebar" data-color="transparent" data-image="<?=base_url()?>assets/img/Sidebar.png">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="logo-text">
                    <img src="<?=base_url()?>assets/img/Logo-2.png" width="200" alt="" />
                </a>
            </div>

            <ul class="nav">
                <li <?=($this->uri->segment(1) == "dashboard")?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(1), "account") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>account/">
                        <i class="pe-7s-user"></i>
                        <p>Account Details</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(1), "order") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>order/">
                        <i class="pe-7s-cart"></i>
                        <p>Order Products</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(1), "income") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>income/">
                        <i class="pe-7s-cash"></i>
                        <p>Income Details</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(1), "binary") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>binary/">
                        <i class="pe-7s-note2"></i>
                        <p>Geneology Binary</p>
                    </a>
                </li>
                <li <?=(strcasecmp($this->uri->segment(1), "mlm") == 0)?  'class="active"' : '' ?>>
                    <a href="<?=base_url()?>mlm/">
                        <i class="pe-7s-link"></i>
                        <p>Geneology MLM</p>
                    </a>
                </li>
                <?php
                    $member_id = $this->session->userdata('user')['member_id'];

                    $date = "%m";
                    $month = mdate($date, time());
                    $point_value = isset($this->Members_Model->get_points($member_id, $month)->point_value) ? $this->Members_Model->get_points($member_id, $month)->point_value : 0;

                    $id = $this->session->userdata('user')['member_id'];
                	$this->Computation_Model->get_pairing($id);
                	$this->Computation_Model->get_pairing_count($id);
                	$this->Computation_Model->compute_pairing_bonus($id);
                    $this->Computation_Model->get_used_pairing_bonus($id);

                    if($point_value != 0) {
                        $this->Computation_Model->total_setter_mlm($member_id);
                    }

                 ?>
                <li>
                    <a class="summary-design">
                        <h4 style="margin-top: 0;">Summary Of Income</h4>
                        Points : <?=$point_value?><Br/>
                        MLM Bonus : ₱ <?=number_format($this->Computation_Model->get_mlm_bonus() !== null? $this->Computation_Model->get_mlm_bonus() : 0, 2)?><Br/>
                        Total : ₱ <?=number_format($this->Computation_Model->get_mlm_bonus() !== null? $this->Computation_Model->get_mlm_bonus() : 0, 2)?>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
