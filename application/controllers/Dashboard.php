<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$this->load->view('dashboard_view', $data);
	}

	public function play() {
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}

		$member_id = $this->session->userdata('user')['member_id'];

		$level = ($this->Members_Model->get_member_level($member_id) !== null)? $this->Members_Model->get_member_level($member_id)->relation_ancestor : '';
		$level = ($level != "")? (strpos($level, '/'))? (count(explode('/', $level))) : '1' : '0';
		$this->Computation_Model->mlm_bonus($member_id, $level);

		$arr = $this->Computation_Model->get_level_arr();
		sort($arr, SORT_NUMERIC);
		echo "U = 1 x 5% x 100 = 50 <br/>";
		$counter = array_count_values($arr);
		foreach ($counter as $key => $value) {
			$key = $key - 1;

			$percentage = 5;
			switch ($key) {
				case '1': $percentage = 5; break;
				case '2': $percentage = 4; break;
				case '3': $percentage = 3; break;
				case '4': $percentage = 2; break;
				default: $percentage = 1; break;
			}

			if(($key) != 15) {
				echo ($key) . " = " . $value . " x " .  $percentage . "% x 1000 = ". ($value*($percentage/100)*1000) ."<Br/>";
			}
		}

		$date = "%m";
		$month = mdate($date, time());

		echo "<br/><Br/>" . $this->Members_Model->get_points($member_id, $month)->point_value;

	}
}
