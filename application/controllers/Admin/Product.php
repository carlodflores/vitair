<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');

		$config['upload_path'] = './assets/img/products/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$config['overwrite'] = false;
		$this->load->library('upload', $config);

	}

    public function index() {
        if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
        $data['products'] = $this->Product_Model->get_all_products();
		$this->load->view('admin/products/all_products_view', $data);
    }

	public function add() {
		if($this->session->userdata('admin') == null){
            redirect(base_url() .'admin/login/');
            exit;
        }

        $this->form_validation->set_rules('txt_product_name', 'Name', 'required');
        $this->form_validation->set_rules('txt_product_description', 'Description', 'required');
        $this->form_validation->set_rules('txt_product_price', 'Price', 'required');
        $this->form_validation->set_rules('txt_product_point_value', 'Points Value', 'required');

        if ($this->form_validation->run()) {
			$img = "";

			if(!empty($_FILES['userfile']['name'])) {
				$this->upload->do_upload();
				$upload_data = $this->upload->data();
				$img = $upload_data['client_name'];
			}

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$product_data = array(
				"product_name" => $this->input->post('txt_product_name'),
				"product_point_value" => $this->input->post('txt_product_point_value'),
				"product_description" => $this->input->post('txt_product_description'),
				"product_price" => $this->input->post('txt_product_price'),
				"product_file" => 'img/products/'.$img,
				"date_created" => $time
			);

			$this->Product_Model->insert_product($product_data);

			redirect(base_url() . 'admin/product/?success=' . md5('add'));
        }

        $data['page_title'] = "Vit-Air Dashboard";
        $data['page_module'] = "Dashboard";
        $this->load->view('admin/products/add_product_view', $data);
	}

    public function edit($product_id) {
        if($this->session->userdata('admin') == null){
            redirect(base_url() .'admin/login/');
            exit;
        }

        $this->form_validation->set_rules('txt_product_name', 'Name', 'required');
        $this->form_validation->set_rules('txt_product_description', 'Description', 'required');
        $this->form_validation->set_rules('txt_product_price', 'Price', 'required');
        $this->form_validation->set_rules('txt_product_point_value', 'Points Value', 'required');


        if ($this->form_validation->run()) {
			$pd = $this->Product_Model->get_product_details($product_id);
			$img = $pd->product_file;

			if(!empty($_FILES['userfile']['name'])) {
				$this->upload->do_upload();
				$upload_data = $this->upload->data();

				$img = $upload_data['client_name'];
			}

			$product_data = array(
				"product_name" => $this->input->post('txt_product_name'),
				"product_point_value" => $this->input->post('txt_product_point_value'),
				"product_description" => $this->input->post('txt_product_description'),
				"product_price" => $this->input->post('txt_product_price'),
				"product_file" => 'img/products/'.$img
			);

			$this->Product_Model->update_product($product_data, $product_id);

			redirect(base_url() . 'admin/product/?success=' . md5('edit'));
        }

        $data['page_title'] = "Vit-Air Dashboard";
        $data['page_module'] = "Dashboard";
        $data['product'] = $this->Product_Model->get_product_details($product_id);
        $this->load->view('admin/products/edit_product_view', $data);
    }

}
