<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()	{

		if($this->session->userdata('admin') !== null){
			redirect(base_url() .'admin/dashboard/');
			exit;
		}

		$this->form_validation->set_rules('txt_member_id', 'Member ID', 'required|callback_loginpassword_check');
		$this->form_validation->set_rules('txt_member_password', 'Password', 'required');

		if($this->form_validation->run()) {
			redirect(base_url().'admin/dashboard/');
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$this->load->view('admin/login/login_view', $data);
	}

	public function loginpassword_check($id) {
		$result = $this->Admin_Model->get_admin_by_id($this->input->post('txt_member_id'));
		$password = $this->input->post('txt_member_password');

		if($result !== null) {
			if($result->admin_password == md5($password)) {
				$user_data_session = array(
					'admin' => $result->admin_id,
					'admin_code' => $result->admin_code
				);
				$this->session->set_userdata('admin', $user_data_session);
				return true;
			} else {
				$this->form_validation->set_message('loginpassword_check', 'Member ID and Password may not be existing.');
				return false;
			}
		} else {
			$this->form_validation->set_message('loginpassword_check', 'Member ID doesn\'t exists.');
			return false;
		}
	}

	public function logout() {
		$this->session->unset_userdata('admin');
		redirect(base_url().'admin/');
	}
}
