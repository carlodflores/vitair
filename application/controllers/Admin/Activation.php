<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activation extends CI_Controller {
	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
	}

	public function index()	{
        if($this->session->userdata('admin') == null){
			redirect(base_url() .'admin/login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$data['activation'] = $this->Activation_Model->get_activation_all();
		$this->load->view('admin/activation/activation_view', $data);
    }

    public function generate() {
        if($this->session->userdata('admin') == null){
            redirect(base_url() .'admin/login/');
            exit;
        }

		$this->form_validation->set_rules('txt_code[]', 'Generated Code', 'required|is_unique[va_activations.activation_code]');
		$this->form_validation->set_rules('txt_pcd_id', 'PCD ID', 'required|callback_pcd_check');

		if($this->form_validation->run()) {
			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			if(is_array($this->input->post('txt_code'))) {
				foreach ($this->input->post('txt_code') as $key => $value) {
					$activation_data = array(
						"activation_code" => $value,
						"franchiser_id" => $this->Franchise_Model->get_franchise_by_id($this->input->post('txt_pcd_id'))->franchiser_id,
						"date_created" => $time
					);

					$this->Activation_Model->insert_activation($activation_data);
				}
			} else {
				$activation_data = array(
					"activation_code" => $this->input->post('txt_code'),
					"franchiser_id" => $this->Franchise_Model->get_franchise_by_id($this->input->post('txt_pcd_id'))->franchiser_id,
					"date_created" => $time
				);

				$this->Activation_Model->insert_activation($activation_data);
			}

			redirect(base_url() . 'admin/activation/generate/?success');
		}

        $data['page_title'] = "Vit-Air Dashboard";
        $data['page_module'] = "Dashboard";
        $this->load->view('admin/activation/generate_view', $data);
    }

	function pcd_check($id) {
		if(count($this->Franchise_Model->get_franchise_by_id($id)) >= 1) {
			return true;
		} else {
			$this->form_validation->set_message('pcd_check', 'PCD ID is invalid.');
			return false;
		}
	}

}
