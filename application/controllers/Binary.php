<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Binary extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}


		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Geneology Binary";
		$data['id'] = $this->session->userdata('user')['member_id'];
		$this->load->view('binary/all_binary_view', $data);
	}

	public function graph() {
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Geneology Binary";
		$data['id'] = $this->session->userdata('user')['member_id'];
		$this->load->view('binary/graphical_binary_view', $data);
	}

}
