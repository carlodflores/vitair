<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function index()	{
		if($this->session->userdata('franchise') == null){
			redirect(base_url() .'franchise/');
			exit;
		}

        $data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$data['orders'] = $this->Order_Model->get_members_order($this->session->userdata('franchise')['franchise_id']);
        $this->load->view('franchise/order/all_orders_view', $data);
    }

	public function done() {
		if($this->session->userdata('franchise') == null){
			redirect(base_url() .'franchise/');
			exit;
		}

        $data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$data['orders'] = $this->Order_Model->get_members_order_done($this->session->userdata('franchise')['franchise_id']);
        $this->load->view('franchise/order/all_orders_view', $data);
	}

	public function info($order_id) {
		if($this->session->userdata('franchise') == null){
			redirect(base_url() .'franchise/');
			exit;
		}

        $data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Dashboard";
		$oi = $this->Order_Model->get_order_info($order_id);
		$data['order'] = $oi;
		$data['content'] = $this->Order_Model->get_all_items_by_group($oi->cart_group);
        $this->load->view('franchise/order/info_order_view', $data);
	}

	public function mark($action, $order_id) {
		if($this->session->userdata('franchise') == null){
			redirect(base_url() .'franchise/');
			exit;
		}

		switch ($action) {
			case 'done':
				$oi = $this->Order_Model->get_order_info($order_id);
				if($oi->order_status != 2) {

					foreach ($this->Order_Model->get_all_items_by_group($oi->cart_group) as $key => $value) {
						$si = $this->Order_Model->get_stock_info($this->session->userdata('franchise')['franchise_id'], $value->product_id);

						if($si->stock_value < $value->cart_quantity) {
							redirect(base_url(). 'franchise/order/info/'. $order_id .'/?fail=' . md5('insustock'));
						}
					}

					foreach ($this->Order_Model->get_all_items_by_group($oi->cart_group) as $key => $value) {
						$si = $this->Order_Model->get_stock_info($this->session->userdata('franchise')['franchise_id'], $value->product_id);
						$stock_data = array(
							"stock_value" => intval($si->stock_value)-intval($value->cart_quantity)
						);

						$this->Order_Model->update_stocks($stock_data, $this->session->userdata('franchise')['franchise_id'], $value->product_id);
					}

					$date = "%Y-%m-%d %H:%i:%s";
					$time = mdate($date, time());

					$point_data = array(
						"member_id" => $oi->member_id,
						"point_value" => $oi->order_total_points,
						"date_created" => $time
					);

					$this->Members_Model->insert_points($point_data);

					$order_data = array(
						"order_status" => "2"
					);

					$this->Order_Model->update_order($order_data, $order_id);

					redirect(base_url(). 'franchise/order/info/'. $order_id .'/?success=' . md5('markdone'));
				} else {
					redirect(base_url(). 'franchise/order/info/'. $order_id .'/?fail=' . md5('markdone'));
				}
				break;

			default:
				# code...
				break;
		}
	}

}
