<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	function __construct(){
	parent::__construct();
		date_default_timezone_set('Asia/Manila');
	}

	public function b1f7ec93c2d2d8a9b6aef4333e7a3fab8()	{

		$date = "%Y-%m-%d %H:%i:%s";
        $time = mdate($date, time());

		echo "Executed " . $time;

        $last_day = date("t", time());
        $dated = "%d";
        $time = mdate($dated, time());
		$this->Computation_Model->reset_computation();
		if($time == 11 || $time == $last_day || $time == 26) {
			foreach ($this->Members_Model->get_all_members() as $key => $value) {
				$id = $value->member_id;

				$this->Computation_Model->get_pairing($id);
				$this->Computation_Model->get_pairing_count($id);
				$this->Computation_Model->compute_pairing_bonus($id);
				$this->Computation_Model->get_used_pairing_bonus($id);
				#$this->Computation_Model->total_setter_mlm($id);
				$this->Computation_Model->generate_payout($id);

				#echo $this->Computation_Model->get_mlm_bonus() ."<br/>";
				$this->Computation_Model->reset_computation();

			}

			$date = "%Y-%m-%d %H:%i:%s";
	        $time = mdate($date, time());
			echo "Success " . $time ;
		}
    }

	function testing($member_id) {
		echo $this->Members_Model->testing($member_id);
	}

}
