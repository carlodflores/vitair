<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()	{

		if($this->session->userdata('user') !== null){
			redirect(base_url() .'dashboard/');
			exit;
		}

		$this->form_validation->set_rules('txt_member_id', 'Member ID', 'required|callback_loginpassword_check');
		$this->form_validation->set_rules('txt_member_password', 'Password', 'required');

		if($this->form_validation->run()) {
			redirect(base_url().'dashboard/');
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$this->load->view('login/login_view', $data);
	}

	public function loginpassword_check($id) {
		$result = $this->Members_Model->check_member_id($this->input->post('txt_member_id'));
		$password = $this->input->post('txt_member_password');

		if($result !== null) {
			if($result->member_password == md5($password)) {
				$admin_info = array(
					'vit_id' => $result->vit_id,
					'member_id' => $result->member_id
				);
				$this->session->set_userdata('user', $admin_info);
				return true;
			} else {
				$this->form_validation->set_message('loginpassword_check', 'Member ID and Password may not be existing.');
				return false;
			}
		} else {
			$this->form_validation->set_message('loginpassword_check', 'Member ID doesn\'t exists.');
			return false;
		}
	}

	public function register() {

		$this->form_validation->set_rules('txt_fname', 'First Name', 'required');
		$this->form_validation->set_rules('txt_lname', 'Last Name', 'required');
		$this->form_validation->set_rules('txt_referral_id', 'Referral ID', 'required|callback_referral_check');
		$this->form_validation->set_rules('txt_pdc_id', 'PDC ID', 'required|callback_pdc_check');
		$this->form_validation->set_rules('txt_activation_code', 'Activation Code', 'required|callback_exist_code');
		$this->form_validation->set_rules('txt_password', 'Password', 'required');
		$this->form_validation->set_rules('txt_re_password', 'Re Password', 'required|matches[txt_password]');
		$this->form_validation->set_rules('txt_position', ' Account Position', 'required|callback_exist_position');

		if($this->form_validation->run()) {

			$date = "%Y-%m-%d %H:%i:%s";
			$time = mdate($date, time());

			$member_data = array(
				"member_fname" => $this->input->post('txt_fname'),
				"member_lname" => $this->input->post('txt_lname'),
				"member_password" => md5($this->input->post('txt_password')),
				"member_referral_id" => $this->Members_Model->check_member_id($this->input->post('txt_referral_id'))->member_id,
				"member_activation_code" => $this->input->post('txt_activation_code'),
				"member_franchise_code" => $this->Franchise_Model->get_franchise_by_id($this->input->post('txt_pdc_id'))->franchiser_id,
				"member_contact" => $this->input->post('txt_contact'),
				"member_address" => $this->input->post('txt_address'),
				"member_email" => $this->input->post('txt_email'),
				"member_birthday" => $this->input->post('txt_birth'),
				"member_beneficiary" => $this->input->post('txt_beneficiary'),
				"member_relation" => $this->input->post('txt_relation'),
				"member_sex" => $this->input->post('txt_sex'),
				"member_civil" => $this->input->post('txt_civil'),
				"member_contact" => $this->input->post('txt_contact'),
				"member_zipcode" => $this->input->post('txt_zip'),
				"member_tin" => $this->input->post('txt_tin'),
				"member_occupation" => $this->input->post('txt_occupation'),
				"member_company" => $this->input->post('txt_company'),
				"date_created" => $time
			);

			$member_id = $this->Members_Model->insert_member($member_data);

			$parent = $this->Members_Model->check_member_id($this->input->post('txt_referral_id'));
			$PCInfo = $this->Members_Model->get_child_info($parent->member_id);
			$child = $this->Members_Model->get_member_info_id($member_id);

			$relation_data = array(
				"relation_parent" => $parent->member_id,
				"relation_child" => $child->member_id,
				"relation_position" => $this->input->post('txt_position'),
				"relation_ancestor" => $PCInfo->relation_ancestor . '/' . $member_id
			);

			$this->Members_Model->insert_relation($relation_data);

			$activation_status = array(
				"activation_status" => 0
			);

			$this->Activation_Model->update_code($this->input->post('txt_activation_code'), $activation_status);

			$point_data = array(
				"member_id" => $member_id,
				"point_value" => "300",
				"date_created" => $time
			);

			$this->Members_Model->insert_points($point_data);

			unset($_POST);

			$admin_info = array(
				'vit_id' => "VITR-" . str_pad($member_id, 4, "0", STR_PAD_LEFT),
				'member_id' => $member_id
			);

			$this->session->set_userdata('user', $admin_info);

			redirect(base_url() . 'login/?success=' . md5('registration'));
		}

		$data['page_title'] = "Vit-Air Dashboard";
		$this->load->view('login/register_view', $data);
	}

	public function exist_code($id) {
		if(count($this->Activation_Model->check_valid_code($this->input->post('txt_activation_code'))) >= 1) {
			return true;
		} else {
			$this->form_validation->set_message('exist_code', 'The Activation Code does\'t exists or its already used.');
			return false;
		}
	}

	public function exist_position($id) {

		if(empty($this->input->post('txt_referral_id'))) {
			return false;
		}

		if(count($this->Members_Model->get_member_downline($this->Members_Model->check_member_id($this->input->post('txt_referral_id'))->member_id)) <= 1) {
			$arr = array();

			foreach ($this->Members_Model->get_member_downline($this->Members_Model->check_member_id($this->input->post('txt_referral_id'))->member_id) as $key => $value) {
				array_push($arr, $value->relation_position);
			}

			if(!in_array($this->input->post('txt_position'), $arr)) {
				return true;
			} else {
				$this->form_validation->set_message('exist_position', 'The Selected Account Position is already filled.');
				return false;
			}
		} else {
			$this->form_validation->set_message('exist_position', 'The Direct Referrer has referred 2 downlines already.');
			return false;
		}
	}

	public function referral_check($id) {
		if(count($this->Members_Model->check_member_id($this->input->post('txt_referral_id'))) >= 1) {
			if(count($this->Members_Model->get_member_downline($this->Members_Model->check_member_id($this->input->post('txt_referral_id'))->member_id)) <= 1) {
				return true;
			} else {
				$this->form_validation->set_message('referral_check', 'The Direct Referrer has referred 2 downlines already.');
				return false;
			}
		} else {
			$this->form_validation->set_message('referral_check', 'Direct Sponsor ID is invalid.');
			return false;
		}
	}

	public function pdc_check($id) {
		if(count($this->Franchise_Model->get_franchise_by_id($id)) >= 1) {
			return true;
		} else {
			$this->form_validation->set_message('pdc_check', 'PDC ID is invalid.');
			return false;
		}
	}

	public function logout() {
		$this->session->unset_userdata('user');
		redirect(base_url());
	}
}
