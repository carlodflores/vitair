<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlm extends CI_Controller {
	public function index()
	{
		if($this->session->userdata('user') == null){
			redirect(base_url() .'login/');
			exit;
		}


		$data['page_title'] = "Vit-Air Dashboard";
		$data['page_module'] = "Geneology MLM";
		$data['id'] = $this->session->userdata('user')['member_id'];
		$this->load->view('mlm/mlm_view', $data);
	}

}
